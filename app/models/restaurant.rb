class Restaurant < ApplicationRecord
  validates :name, presence: true
  has_and_belongs_to_many :dishes,
                          class_name: 'Dish',
                          join_table: :dish_restaurants,
                          autosave: true, uniq: true

  def as_json(*)
    {
      id: id,
      name: name
    }
  end
end
