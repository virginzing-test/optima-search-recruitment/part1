class Dish < ApplicationRecord
  validates :name, presence: true
  has_and_belongs_to_many :restaurants,
                          class_name: 'Restaurant',
                          join_table: :dish_restaurants,
                          autosave: true, uniq: true

  def as_json(*)
    {
      id: id,
      name: name
    }
  end
end
