class RestaurantsController < ApplicationController
  before_action :set_restaurant, only: [:show, :dishes]

  # GET /restaurants
  def index
    @restaurants = Restaurant.all

    render json: @restaurants
  end

  # GET /restaurants/1
  def show
    return render :json, status: 404 if @restaurant.nil?

    render json: @restaurant
  end

  def dishes
    return render :json, status: 404 if @restaurant.nil?

    @dishes = @restaurant.dishes

    render json: @dishes
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_restaurant
      @restaurant = Restaurant.find_by(id: params[:id])
    end
end
