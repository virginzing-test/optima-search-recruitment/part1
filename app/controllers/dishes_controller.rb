class DishesController < ApplicationController
  before_action :set_dish, only: [:show, :restaurants]

  # GET /dishes
  def index
    @dishes = Dish.all

    render json: @dishes
  end

  # GET /dishes/1
  def show
    return render :json, status: 404 if @dish.nil?

    render json: @dish
  end

  def restaurants
    return render :json, status: 404 if @dish.nil?

    @restaurants = @dish.restaurants

    render json: @restaurants
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_dish
      @dish = Dish.find_by(id: params[:id])
    end
end
