# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Dish.create(name: 'Pizza')
Dish.create(name: 'Steak')
Dish.create(name: 'Burger')
Dish.create(name: 'Bread')
Dish.create(name: 'French fries')
Dish.create(name: 'Sausage')
Dish.create(name: 'Sandwich')
Dish.create(name: 'Salad')
Dish.create(name: 'Nachos')
Dish.create(name: 'Ham')

Restaurant.create(name: 'KFC')
Restaurant.last.dishes += Dish.all.sample(5)

Restaurant.create(name: 'The Pizza')
Restaurant.last.dishes += Dish.all.sample(6)

Restaurant.create(name: 'McDonald’s')
Restaurant.last.dishes += Dish.all.sample(7)

Restaurant.create(name: 'Taco Bell')
Restaurant.last.dishes += Dish.all.sample(8)

Restaurant.create(name: 'Burger King')
Restaurant.last.dishes += Dish.all.sample(9)
