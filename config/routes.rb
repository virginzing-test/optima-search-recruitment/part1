Rails.application.routes.draw do
  get 'dishes', to: 'dishes#index'
  get 'dishes/:id', to: 'dishes#show'
  get 'dishes/:id/restaurants', to: 'dishes#restaurants'

  get 'restaurants', to: 'restaurants#index'
  get 'restaurants/:id', to: 'restaurants#show'
  get 'restaurants/:id/dishes', to: 'restaurants#dishes'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
