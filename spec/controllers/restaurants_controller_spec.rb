require 'rails_helper'

RSpec.describe 'Restaurants request', :type => :request do
  before do
    Restaurant.create(name: 'test 1')
    Restaurant.create(name: 'test 2')

    Dish.create(name: 'test 1')
    Dish.create(name: 'test 2')

    Restaurant.last.dishes += Dish.all
  end

  describe 'GET /restaurants' do
    it 'return list all restaurants' do
      get '/restaurants'
      expect(response.body).to eq(Restaurant.all.to_json)
    end
  end

  describe 'GET /restaurants/:id' do
    context 'case restaurant :id exist' do
      it 'return restaurant :id detail' do
        get "/restaurants/#{Restaurant.last.id}"
        expect(response.body).to eq(Restaurant.last.to_json)
      end
    end

    context 'case restaurants :id not found' do
      it 'return status 404' do
        get '/restaurants/0'
        expect(response.status).to eq(404)
      end
    end
  end

  describe 'GET /restaurants/:id' do
    context 'case restaurant :id exist' do
      it "return list restaurant :id's dishes" do
        get "/restaurants/#{Restaurant.last.id}/dishes"
        expect(response.body).to eq(Restaurant.last.dishes.to_json)
      end
    end

    context 'case restaurant :id not found' do
      it 'return status 404' do
        get '/restaurants/0/dishes'
        expect(response.status).to eq(404)
      end
    end
  end
end