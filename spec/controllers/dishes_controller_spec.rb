require 'rails_helper'

RSpec.describe 'Dishes request', :type => :request do
  before do
    Dish.create(name: 'test 1')
    Dish.create(name: 'test 2')

    Restaurant.create(name: 'test 1')
    Restaurant.create(name: 'test 2')

    Dish.last.restaurants += Restaurant.all
  end

  describe 'GET /dishes' do
    it 'return list all dishes' do
      get '/dishes'
      expect(response.body).to eq(Dish.all.to_json)
    end
  end

  describe 'GET /dishes/:id' do
    context 'case dish :id exist' do
      it 'return dish :id detail' do
        get "/dishes/#{Dish.last.id}"
        expect(response.body).to eq(Dish.last.to_json)
      end
    end

    context 'case dish :id not found' do
      it 'return status 404' do
        get '/dishes/0'
        expect(response.status).to eq(404)
      end
    end
  end

  describe 'GET /dishes/:id/restaurants' do
    context 'case dish :id exist' do
      it "return list dish :id's restaurants" do
        get "/dishes/#{Dish.last.id}/restaurants"
        expect(response.body).to eq(Dish.last.restaurants.to_json)
      end
    end

    context 'case dish :id not found' do
      it 'return status 404' do
        get '/dishes/0/restaurants'
        expect(response.status).to eq(404)
      end
    end
  end
end