require 'rails_helper'

RSpec.describe DishRestaurant, type: :model do
  it { should belong_to(:dish) }
  it { should belong_to(:restaurant) }
end
